/*
   Wednesday, 05 June 201903:06:55
   User: 
   Server: DESKTOP-3FG40AL\SQLEXPRESS
   Database: Pages
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.dtlPages
	(
	uidId uniqueidentifier NOT NULL,
	uidParentId uniqueidentifier NULL,
	vchPageName varchar(50) NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.dtlPages ADD CONSTRAINT
	PK_dtlPages PRIMARY KEY CLUSTERED 
	(
	uidId
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.dtlPages SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
