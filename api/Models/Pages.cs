using System;
using System.Collections.Generic;

namespace bgAssessment.Models
{
    public class Pages
    {
        public Guid Id {get;set;}
        public Guid? ParentId{get;set;}
        public string PageName {get;set;}
        public List<Pages> ChildPages {get;set;}
    }
}