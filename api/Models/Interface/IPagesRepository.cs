using System;
using System.Collections.Generic;

namespace bgAssessment.Models.Interface
{
    public interface IPagesRepository
    {
        IEnumerable<Pages> GetAll();
        Pages Get(Guid Id);
    }
}