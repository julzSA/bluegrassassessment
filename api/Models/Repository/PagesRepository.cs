using System;
using System.Collections.Generic;
using System.Linq;
using bgAssessment.Models.Entities;
using bgAssessment.Models.Interface;

namespace bgAssessment.Models.Repository
{
    public class PagesRepository : IPagesRepository
    {
        private readonly pagesDbContext _context;  
        public PagesRepository(pagesDbContext context)  
        {  
            _context = context;  
        } 

        public Pages Get(Guid Id)
        {
            return _context.Pages  
                .Where(p => p.uidId == Id)  
                .Select(s => new Pages()
                {
                    Id = s.uidId,
                    ParentId = s.uidParentId,
                    PageName = s.vchPageName
                }).FirstOrDefault(); 
        }

        public IEnumerable<Pages> GetAll()
        {
             var parentId = _context.Pages
                    .Where(w => w.uidParentId == null)
                    .Select(s => s.uidId)
                    .FirstOrDefault();

            return GetChildren(_context.Pages.ToList(), parentId);
        }

        private List<Pages> GetChildren(List<PagesEntity> Pages, Guid parentId)
        {
            List<Pages> pages = new List<Pages>();

            foreach (var item in Pages.Where(x => x.uidParentId.Equals(parentId)))
            {
                pages.Add(new Pages
                {
                    Id = item.uidId,
                    ParentId = item.uidParentId,
                    PageName = item.vchPageName,
                    ChildPages = GetChildren(Pages, item.uidId)
                });
            }
            return pages;
        }
    }
}