using Microsoft.EntityFrameworkCore;

namespace bgAssessment.Models.Entities
{
    public class pagesDbContext : DbContext
    {
        public pagesDbContext(DbContextOptions<pagesDbContext> options) : base(options)
        {
        }

        public DbSet<PagesEntity> Pages { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                #warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer(@"server=.\\SQL2014;database=Pages;User ID=user;Password=password;Trusted_Connection=True;");
            }
        }

    }
}