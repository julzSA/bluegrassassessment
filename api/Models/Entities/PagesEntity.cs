using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace bgAssessment.Models.Entities
{
    [Table("dtlPages")]
    public class PagesEntity
    {
        [Key]
        public Guid uidId {get;set;}
        public Guid? uidParentId {get;set;}
        public string vchPageName {get;set;}
    }
}