using System;
using System.Collections.Generic;
using System.Linq;
using bgAssessment.Models;
using bgAssessment.Models.Interface;
using Microsoft.AspNetCore.Mvc;

namespace bgAssessment.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PagesController : ControllerBase
    {
        IPagesRepository _pagesRepo;

        /// <summary>
        /// Pages Web Api
        /// </summary>
        public PagesController(IPagesRepository pagesRepo)
        {
            _pagesRepo = pagesRepo;
        }
        

        /// <summary>
        /// Get All Pages.
        /// </summary>
        /// <returns>All Pages</returns>
        /// <response code="201">Returns all pages</response>
        /// <response code="400">If the item is null</response>  
        // GET api/pages
        [HttpGet]
        public ActionResult<IEnumerable<Pages>> Get()
        {
            return _pagesRepo.GetAll().ToList();
        }

        /// <summary>
        /// Get a Page using its id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>A requested Page</returns>
        /// <response code="201">Returns the requested page</response>
        /// <response code="400">If the item is null</response>    
        // GET api/pages/2ADEA345-7F7A-4313-87AE-F05E8B2DE678
        [HttpGet("{id}")]
        public ActionResult<Pages> Get(Guid id)
        {
            return _pagesRepo.Get(id);
        }
    }
}