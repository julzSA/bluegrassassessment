# BlueGrass Assessment

Bluegrass Assessment project.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

git clone git@bitbucket.org:julzSA/bluegrassassessment.git

### Prerequisites

What things you need to install the software and how to install them

```
Visual Studio
SQL Server Instance
```

### Installing

Setup db on SQL Instance, by either running script 01_schema_dtlPages.sql and then 02_seed_data.sql in the db_scripts folders or publishing the database project to your sql instance.

Update DB config setting in appsettings.json

Run project.

## Authors

* **Julian Carolissen** - *Initial Work* - [JulzSA](https://github.com/JulzSA)